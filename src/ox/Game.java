package ox;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Game {
	private Scanner kb = new Scanner(System.in);
	private int row ;
	private int col ;
	Table table = null ;
	Player o =null;
	Player x = null;
	public Game() {
		this.o = new Player('o');
		this.x = new Player('x');
	}
	public void run() {
		while(true) {
			this.runOnce();
			if(!askContinue()){
				return;
			}
		}
	}
	private boolean askContinue() {
		while(true) {
			System.out.println("Continue y/n ?");
			String ans = kb.next();
			if(ans.equals("n")) {
				return false;
			}else if(ans.equals("y")) {
				return true; 
			}
		}
	}
	private int getRandomNumber(int min,int max) {
		return (int)((Math.random()*(max-min))+min);
	}
	public void newGame() {
		if(getRandomNumber(1,100)%2==0) {
			this.table = new Table(o,x);
		}else {
			this.table = new Table(x,o);
		}
		
	}
	public void runOnce() {
		this.showWelcome();
		this.newGame();
		while(true) {
			this.showTable();
			this.showTurn();
			this.inputRowCol();
			if(table.checkWin()) {
				showResult();
				this.showStat();
				return ;
			}
			
			table.switchPlayer();
		}
		
	}

	private void showResult() {
		if(table.getWinner()!=null) {
			showWin();				
		}else {
			showDraw();
		}
		
	}
	private void showDraw() {
		System.out.println("Drawwww");
		
	}
	private void showWin() {
		System.out.println(table.getWinner().getName()+" Winnnn");
		
	}
	private void showStat() {
		System.out.println(o.getName()+"(win,lose,draw):"+o.getWin()+o.getLose()+o.getDraw());
		System.out.println(x.getName()+"(win,lose,draw):"+x.getWin()+x.getLose()+x.getDraw());

	}
	private void inputRowCol() {
		try {
			while(true) {
				this.input();
				if(table.setRowCol(row,col)) {
					return ;
				}else {
					System.out.println("this row col is not empty");
				}
			}
		}catch(ArrayIndexOutOfBoundsException exception) {
		    System.out.println("ArrayIndexOutOfBoundsException exception");
		}
	}
	private void input() {
		while(true) {
			try {
				 System.out.print("Please in put row col: ");
					this.row = kb.nextInt();
					this.col = kb.nextInt();
					if(kb.hasNext()) {
						System.out.println("Please in put number!!!");
					}
			}catch(InputMismatchException iE){
				kb.next();
				System.out.println("Please in put number!!!"); 
			}
	   
		}
	}

	private void showTurn() {
		System.out.println(table.getCurrentPlayer().getName()+" Turn");
		
	}

	private void showTable() {
		char[][] data=this.table.getData();
		for(int row=0; row<data.length;row++) {
			for(int col = 0;col<data[row].length;col++) {
				System.out.print(" "+data[row][col]);
			}
			System.out.println(" ");
		}
	}

	private void showWelcome() {
		System.out.println("Welcome OX Game!!!");
		
	}
	
	

}

